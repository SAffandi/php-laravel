@extends('layout.master')
@section('judul')
    Halaman List Cast 
@endsection
@section('content')
<a href="/cast/create" class="btn btn-primary btn-sm my-2">Tambah Cast</a>
<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">Nomor</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Biodata</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key=>$item)
            <tr>
                <td>{{$key+1}}</td>
                <td>{{$item->nama}}</td>
                <td>{{$item->umur}}</td>
                <td>{{$item->bio}}</td>
                <td>
                    <a href="/cast/{{$item->id}}" class="btn btn-sm btn-info">Detail</a>
                    <a href="/cast/{{$item->id}}/edit" class="btn btn-sm btn-info">Edit</a>
                </td>
            </tr>
        @empty
            <tr>
                <td>
                    Data Cast kosong
                </td>
            </tr>
        @endforelse
      
      </tr>
    </tbody>
  </table>
@endsection