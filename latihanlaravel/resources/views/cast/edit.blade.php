@extends('layout.master')
@section('judul')
    Halaman Edit Cast
@endsection
@section('content')

<div>
    <h2>Tambah Data</h2>
        <form action="/cast/{{$cast->id}}" method="POST">
            @csrf
            @method('put')
            <div class="form-group">
                <label>Nama Pemain Film</label>
                <input type="text" value="{{$cast->nama}}" class="form-control" name="nama" placeholder="Masukkan nama pemain film">
            </div>
            @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            <div class="form-group">
                <label>Umur Pemain Film</label>
                <input type="number" value="{{$cast->umur}}" class="form-control" name="umur" placeholder="Masukkan umur pemain film">
            </div>
            @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            <div class="form-group">
                <label>Biodata Pemain Film</label>
                <input type="text" value="{{$cast->bio}}" class="form-control" name="bio" placeholder="Masukkan biodata pemain film">
            </div>
            @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
</div>
@endsection