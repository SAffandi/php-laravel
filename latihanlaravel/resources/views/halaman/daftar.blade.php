@extends('layout.master')
@section('judul')
<h1>Buat Account Baru!</h1>
@endsection
@section('content')
<form action="/kirim" method="POST">
    @csrf
    <label>First name:</label><br>
    <input type="text" name="NamaDepan"><br>
    <label>Last name:</label><br>
    <input type="text" name="NamaBelakang"><br><br>
    <label>Gender:</label><br>
    <input type="radio" id="male" name="Gender" value="Male">
    <label for="male">Male</label><br>
    <input type="radio" id="female" name="Gender" value="Female">
    <label for="female">Female</label><br>
    <input type="radio" id="other" name="Gender" value="Other">
    <label for="other">Other</label><br><br>
    <label>Nationality</label><br>
    <select name="WargaNegara">
        <option value="Indonesia"> Indonesia </option>
        <option value="Other"> Other</option>
    </select> <br> <br>
    <label>Language Spoken:</label><br>
    <input type="checkbox"> Bahasa Indonesia
    <br>
    <input type="checkbox"> English
    <br>
    <input type="checkbox"> Other
    <br><br>
    <label>Bio:</label> <br>
    <textarea name="Biodata" cols="30" rows="10"></textarea><br><br>
    <input type="submit" value="kirim">
</form>
@endsection


