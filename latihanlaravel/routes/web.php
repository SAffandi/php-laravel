<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', 'HomeController@dashboard');
Route::get('/pendaftaran', 'AuthController@daftar');
Route::post('/kirim', 'AuthController@kirim');

Route::get('/data-table', function () {
    return view('halaman.data-table');
});

//CRUD CAST
//Create
//Form input data Cast
Route::get('/cast/create', 'CastController@create');
//Menyimpan data ke database
Route::post('/cast', 'CastController@store');
//Read
//Menampilkan semua data di tabel cast
Route::get('/cast', 'CastController@index');
//Menampilkan cast berdasarkan id
Route::get('/cast/{cast_id}', 'CastController@show');
//Update
//Form edit data cast
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
//untuk Update data berdasarkan id
Route::put('/cast/{cast_id}', 'CastController@update');
