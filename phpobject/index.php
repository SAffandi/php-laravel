<?php

require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$hewan = new animal("Shaun");

echo "Nama hewan : " . $hewan->type . "<br>";
echo "Kaki hewan : " . $hewan->kaki . "<br>";
echo "Berdarah dingin : " . $hewan->coldBlood . "<br><br>";

$hewankodok = new kodok("buduk");

echo "Nama hewan : " . $hewankodok->type . "<br>";
echo "Kaki hewan : " . $hewankodok->kaki . "<br>";
echo "Berdarah dingin : " . $hewankodok->coldBlood . "<br>";
echo "Jump : " . $hewankodok->jump("Hop Hop") . "<br><br>";


$hewanmonyet = new monyet("Kera Sakti");

echo "Nama hewan : " . $hewanmonyet->type . "<br>";
echo "Kaki hewan : " . $hewanmonyet->kaki . "<br>";
echo "Berdarah dingin : " . $hewanmonyet->coldBlood . "<br>";
echo "Yell : " . $hewanmonyet->yell("Auooo") . "<br>";
